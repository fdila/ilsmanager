FROM debian:bookworm

ARG ILS_ENABLE_XDEBUG

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get install --yes \
      composer \
      php-cli \
      php-mysql \
      php-pdo \
      php-gd \
      php-dom \
      php-curl \
      php-bcmath \
      php-xdebug \
 && apt-get clean

# Activate PHP xdebug. True as default. False on GitLab CI.
RUN if [ "$ILS_ENABLE_XDEBUG" != 0 ]; then echo "xdebug.mode=debug,develop" >> /etc/php/8.2/mods-available/xdebug.ini; fi
RUN if [ "$ILS_ENABLE_XDEBUG" != 0 ]; then echo "xdebug.client_host=host.docker.internal" >> /etc/php/8.2/mods-available/xdebug.ini; fi
RUN if [ "$ILS_ENABLE_XDEBUG" != 0 ]; then echo "xdebug.start_with_request=yes" >> /etc/php/8.2/mods-available/xdebug.ini; fi
RUN if [ "$ILS_ENABLE_XDEBUG" != 0 ]; then echo "xdebug.client_port=9003" >> /etc/php/8.2/mods-available/xdebug.ini; fi
RUN if [ "$ILS_ENABLE_XDEBUG" != 0 ]; then echo "xdebug.idekey=PHPSTORM" >> /etc/php/8.2/mods-available/xdebug.ini; fi

COPY ./entrypoint.sh /entrypoint.sh

WORKDIR /var/www/html

EXPOSE 8000/tcp

ENTRYPOINT [ "/entrypoint.sh" ]
