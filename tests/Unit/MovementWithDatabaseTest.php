<?php

namespace Tests\Unit;

use App\Account;
use App\AccountRow;
use App\Actions\MovementUnicreditRefreshParsingNotes;
use App\Bank;
use App\Exceptions\CannotOverwriteMovementIdentifierException;
use App\Movement;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MovementWithDatabaseTest extends TestCase
{
    // This allows SQLite
    use DatabaseMigrations;

    /**
     * Test the Movement unique finder on PayPal.
     *
     * @test
     */
    public function movement_paypal_already_tracked(): void
    {
        $bank = new Bank;
        $bank->name = 'Test Bank from movement_unique_finder()';
        $bank->type = 'paypal';
        $bank->identifier = 'test-bank-paypal-unique-finder-' . rand();
        $bank->save();

        // Create a base PayPal Movement.
        $movement = new Movement;
        $movement->bank_id = $bank->id;
        $movement->amount = 0.99;
        $movement->identifier = 'asdlol-paypal-id';
        $movement->date = '2001-02-03';
        $movement->notes = 'Test PayPal movement from test method: ' . __METHOD__;
        $movement->save();

        // Propose a duplicated PayPal Movement that will be recognized by its bank identifier (and only that).
        $movement2 = new Movement;
        $movement2->bank_id = $movement->bank_id;
        $movement2->identifier = $movement->identifier;
        $movement2->amount = 0.98; // Change the amount a bit
        $movement2->date = '2001-02-04'; // Change the date a bit
        $movement2->notes = 'Test PayPal movement bla bla other note from test method: ' . __METHOD__;
        $this->assertEquals(true, $movement2->alreadyTracked(), 'Test PayPal movement already tracked');
        $this->assertEquals($movement->id, $movement2->alreadyTrackedMovement()->id, 'Test PayPal movement already tracked object ID');

        // Propose a very-similar PayPal movement but with different bank identifier.
        $movement3 = new Movement;
        $movement3->bank_id = $movement->bank_id;
        $movement3->amount = $movement->amount;
        $movement3->identifier = 'something-different-paypal-id-' . rand();
        $movement3->date = $movement->date;
        $movement3->notes = $movement->notes;
        $this->assertEquals(false, $movement3->alreadyTracked(), 'Test PayPal movement not tracked');
        $this->assertEquals(null, $movement3->alreadyTrackedMovement(), 'Test PayPal movement not tracked object');

        // Cleanup
        $movement->delete();
        $bank->delete();
    }

    /**
     * Test the Movement unique finder on Unicredit.
     * Note that Unicredit movements have not an identifier.
     *
     * @test
     */
    public function movement_unicredit_already_tracked(): void
    {
        $bank = new Bank;
        $bank->name = 'Test Bank from movement_unique_finder()';
        $bank->type = 'unicredit';
        $bank->identifier = 'test-bank-unicredit-unique-finder-' . rand();
        $bank->save();

        // Create a base Unicredit Movement (without identifier - since Unicredit does not share identifiers).
        $movement = new Movement;
        $movement->bank_id = $bank->id;
        $movement->amount = 0.99;
        $movement->date = '2001-02-03';
        $movement->notes = 'Test Unicredit movement from test method: ' . __METHOD__;
        $movement->save();

        // Propose a duplicated Unicredit Movement.
        $movement2 = new Movement;
        $movement2->bank_id = $movement->bank_id;
        $movement2->amount = $movement->amount;
        $movement2->date = $movement->date;
        $movement2->notes = $movement->notes;
        $this->assertEquals(true, $movement2->alreadyTracked(), 'Test Unicredit movement already tracked');
        $this->assertEquals($movement->id, $movement2->alreadyTrackedMovement()->id, 'Test Unicredit movement already tracked object');

        // Propose another Unicredit Movement that must be considered different.
        $movement3 = new Movement;
        $movement3->bank_id = $movement->bank_id;
        $movement3->amount = $movement->amount;
        $movement3->date = '2002-03-04'; // This date is different from the original movement
        $movement3->notes = 'Test different Unicredit asdlol new movement from test method: ' . __METHOD__;
        $this->assertEquals(false, $movement3->alreadyTracked(), 'Test Unicredit movement not tracked');
        $this->assertEquals(null, $movement3->alreadyTrackedMovement(), 'Test Unicredit movement not tracked object');

        // Cleanup
        $movement->delete();
        $bank->delete();
    }

    /**
     * @test
     */
    public function movement_unicredit_parse_notes(): void
    {
        $test_asdlol = 'test-asdlol-' . __METHOD__ . rand();

        // Have at least one bank.
        $bank = new Bank;
        $bank->name = 'Test Unicredit Bank from ' . $test_asdlol;
        $bank->identifier = $test_asdlol;
        $bank->type = 'unicredit';
        $bank->save();

        // Have exactly one test "Bank fees".
        // It's quite important to just have one, since multiple code parts
        // looks for the first of these, so this must be one to be reproducible.
        $bank_account_for_bank_fees = Account::where('bank_costs', true)->first();
        $bank_account_for_bank_fees_created = false;
        if (! $bank_account_for_bank_fees) {
            $bank_account_for_bank_fees = new Account;
            $bank_account_for_bank_fees->name = 'Spese bancarie ' . $test_asdlol;
            $bank_account_for_bank_fees->bank_costs = 1;
            $bank_account_for_bank_fees->save();
            $bank_account_for_bank_fees_created = true;
        }
        $bank_account_for_bank_fees_id = $bank_account_for_bank_fees->id;

        // Have at least one "User fees" to make AccountRowObserver happy,
        // since it may call Config::feesAccount()->id, and without any of these
        // can crash. So, this is always created and always cleaned up.
        $bank_account_for_user_fees = new Account;
        $bank_account_for_user_fees->name = 'Quota associatie ' . $test_asdlol;
        $bank_account_for_user_fees->fees = 1;
        $bank_account_for_user_fees->save();
        $bank_account_for_user_fees_id = $bank_account_for_bank_fees->id;

        // This is a real-world example with bank commissions.
        $m = new Movement;
        $m->notes = 'Test 1 with bank commissions. DISPOSIZIONE DI BONIFICO BONIFICO SEPA A  ROMA2LUG - TOR VERGATA LINUX USERS PER  Rimborso spese  68 COMM              0,74 SPESE              0,00 TRN 12345';
        $m->amount = 9.00;
        $m->amount_original = null;
        $m->bank_id = $bank->id;
        $m->date = '2024-05-06 07:08:09';
        $this->parse_movement_notes_compare_result($m, [
            'amount_original' => '9.74',
            'amount' => $m->amount,
            'identifier' => '12345',
            'ar' => [
                'amount' => '-0.74',
                'account_id' => $bank_account_for_user_fees_id,
            ],
        ]);

        // This is a real-world example without bank commissions.
        $m = new Movement;
        $m->notes = 'Test 2 simple without bank commissions and without identifier. DISPOSIZIONE DI BONIFICO BONIFICO SEPA A  ROMA2LUG - TOR VERGATA LINUX USERS PER  Rimborso spese lol';
        $m->amount = 12.34;
        $m->amount_original = null;
        $m->bank_id = $bank->id;
        $m->date = '2024-05-06 07:08:09';
        $this->parse_movement_notes_compare_result($m, [
            'amount_original' => null,
            'amount' => $m->amount,
            'identifier' => null,
            'ar' => null,
        ]);

        $m = new Movement;
        $m->notes = 'Test 3 with spese. DISPOSIZIONE DI BONIFICO BONIFICO SEPA A  ROMA2LUG - TOR VERGATA LINUX USERS PER  Rimborso spese  68 COMM              0,00 SPESE              1,23 TRN 543210';
        $m->amount = 100.00;
        $m->amount_original = null;
        $m->bank_id = $bank->id;
        $m->date = '2024-05-06 07:08:09';
        $this->parse_movement_notes_compare_result($m, [
            'amount_original' => '101.23',
            'amount' => $m->amount,
            'identifier' => '543210',
            'ar' => [
                'amount' => '-1.23',
                'account_id' => $bank_account_for_user_fees_id,
            ],
        ]);

        // This is not a real-world example, but I assume that SPESE+COMM
        // should be summed.
        $m = new Movement;
        $m->notes = 'Test 4 with comm and spese. DISPOSIZIONE DI BONIFICO BONIFICO SEPA A  ROMA2LUG - TOR VERGATA LINUX USERS PER  Rimborso spese  68 COMM              1,00 SPESE              0,23 TRN 999';
        $m->amount = '-201.23';
        $m->amount_original = null;
        $m->bank_id = $bank->id;
        $m->date = '2024-05-06 07:08:09';
        $this->parse_movement_notes_compare_result($m, [
            'amount_original' => '-200.00',
            'amount' => $m->amount,
            'identifier' => '999',
            'ar' => [
                'amount' => '-1.23',
                'account_id' => $bank_account_for_user_fees_id,
            ],
        ]);

        // This is a real-world example with bank commissions and already-existing AccountRow
        // that will be refreshed, without creating a new duplicated AccountRow.
        $m = new Movement;
        $m->notes = 'Test 5 refreshing bank commissions, persisted. DISPOSIZIONE DI BONIFICO BONIFICO SEPA A  ROMA2LUG - TOR VERGATA LINUX USERS PER  Rimborso spese  68 COMM              0,74 SPESE              0,00 TRN 1111111';
        $m->amount = 99.26;
        $m->amount_original = null;
        $m->bank_id = $bank->id;
        $m->date = '2024-05-06 07:08:09';
        $m->save();
        $ar = new AccountRow;
        $ar->movement_id = $m->id;
        $ar->amount = '0.01'; // Mistake that will be refreshed with 0.74.
        $ar->account_id = $bank_account_for_user_fees_id;
        $ar->notes = 'Commissioni Unicredit con nota probabilmente da mantenere';
        $ar->save();
        $this->parse_movement_notes_compare_result($m, [
            'amount_original' => '100.00',
            'amount' => $m->amount,
            'identifier' => '1111111',
            'ar' => [
                'id' => $ar->id,
                'amount' => '-0.74',
                'account_id' => $bank_account_for_user_fees_id,
            ],
        ]);
        $ar->delete();
        $m->delete();

        // Identifiers are not supposed to be replaced. Cause a crash.
        $m = new Movement;
        $m->notes = 'Test 6 trying to overwrite identifier by mistake. bla bla bla TRN 2222222';
        $m->amount = 1;
        $m->amount_original = null;
        $m->identifier = '1111111';
        $m->bank_id = $bank->id;
        $m->date = '2024-05-06 07:08:09';
        $m->save();
        $e = null;
        try {
            $action = $this->parse_movement_notes_compare_result($m, []);
        } catch (CannotOverwriteMovementIdentifierException $e) {
            // Everything is OK. This is supposed to crash.
        }
        $this->assertNotNull($e, 'The test n. 6 was supposed to crash with a CannotOverwriteMovementIdentifierException exception.');
        $m->delete();
        unset($action);
        unset($e);

        $mfake = new Movement;
        $mfake->notes = 'Test 7 Update an already-existing movement that has multiple AccountRows and one with bank costs to be updated. bla bla bla COMM              0,00 SPESE              1,23 TRN 7777777';
        $mfake->amount = '3.77'; // This is supposed to be kept as-is.
        $mfake->amount_original = null;  // This is going to be replaced with '5.00' (amount+bank, that is 3.77+1.23)
        $mfake->identifier = null; // This is supposed to be populated.
        $mfake->bank_id = $bank->id; // This is supposed to be kept as-is.
        $mfake->date = '2024-05-06 07:08:09'; // This is supposed to be kept as-is.
        //$mfake->save(); This must be transient for this test.
        $m = new Movement;
        $m->notes = 'asdlol'; // This is supposed to be replaced with $mfake->notes.
        $m->amount = '3.77'; // This is supposed to be kept as-is.
        $m->amount_original = null;  // This is going to be replaced with '5.00' (amount+bank, that is 3.77+1.23)
        $m->identifier = '7777777'; // This is supposed to be kept as-is.
        $m->bank_id = $bank->id; // This is supposed to be kept as-is.
        $m->date = '2024-05-06 07:08:10'; // This is supposed to be kept as-is.
        $m->save();
        $a = new Account;
        $a->name = 'Acquisti extra';
        $ar1 = new AccountRow;
        $ar1->movement_id = $m->id;
        $ar1->account_id = $a->id;
        $ar1->amount = '0.01'; // This is supposed to be kept as-is.
        $ar1->notes = 'Acquisto extra'; // This is supposed to be kept as-is.
        $ar1->save();
        $ar2 = new AccountRow;
        $ar2->movement_id = $m->id;
        $ar2->account_id = Account::bankCostsId(); // This is supposed to be kept as-is.
        $ar2->amount = '-3.21'; // This is supposed to be replaced with '-1.23'.
        $ar2->notes = 'Commissioni Unicredit';
        $ar2->save();
        $this->parse_movement_notes_compare_result($mfake, [
            'id' => $m->id,
            'amount' => $m->amount,
            'amount_original' => '5.00',
            'identifier' => $m->identifier,
            'notes' => $mfake->notes,
            'ar' => [
                'id' => $ar2->id,
                'amount' => '-1.23',
                'account_id' => $ar2->account_id,
            ],
        ], [
            'pleaseExpandExistingDuplicate' => true,
        ]);
        // Make sure that the first AccountRow unrelated to bank costs was not changed.
        $ar1->refresh();
        $this->assertEquals($ar1->amount, '0.01');
        $this->assertEquals($ar1->notes, 'Acquisto extra');
        $this->assertEquals($ar1->account_id, $a->id);
        $ar1->delete();
        $ar2->delete();
        $a->delete();
        $m->delete();
        
        // Cleanup.
        if ($bank_account_for_bank_fees_created) {
            $bank_account_for_bank_fees->delete();
        }
        $bank_account_for_user_fees->delete();
        $bank->delete();
    }

    /**
     * Parse the movement notes, compare the result, and cleanup.
     *
     * @param  Movement  $m  Input movement. It should have some 'notes'.
     *                       It will be persisted then deleted.
     *                       A related AccountRow may be persisted and deleted too.
     * @param  array  $expected  Expected data. Like:
     *                           'id' (int) => expected Movement.id
     *                           'identifier' (string) => expected Movement.identifier
     *                           'amount' (string) => expected Movement.amount
     *                           'amount_original' (string) => expected Movement.account_original
     *                           'ar' (array) => Expected AccountRow data. Including: id, amount, account_id.
     * @param  args  $args  Some optional arguments, like 'pleaseExpandExistingDuplicate' => true, if you want
     *                      to avoid creation of duplicate elements, and expand already-existing stuff.
     *                      Default is false, so, may create duplicates.
     */
    private function parse_movement_notes_compare_result(Movement $m, array $expected, array $args = []): MovementUnicreditRefreshParsingNotes
    {
        $test_name_prefix = "Test movement with notes: {$m->notes} Check ";

        // This should create an AccountRow.
        // This does not persist the Movement.
        $action = new MovementUnicreditRefreshParsingNotes($m);
        $action->handle();
        //In these unit tests we don't want

        $pleaseExpandExistingDuplicate = $args['pleaseExpandExistingDuplicate'] ?? false;
        if ($pleaseExpandExistingDuplicate) {
            $action->pleaseExpandExistingDuplicate();
        }

        $ar = $action->getArBankCosts();
        $m = $action->getMovement();

        if (array_key_exists('id', $expected)) {
            $this->assertEquals($expected['id'], $m->id, $test_name_prefix . 'Movement.id');
        }

        if (array_key_exists('notes', $expected)) {
            $this->assertEquals($expected['notes'], $m->notes, $test_name_prefix . 'Movement.notes');
        }

        $this->assertEquals($expected['identifier'], $m->identifier, $test_name_prefix . 'Movement.identifier');
        $this->assertEquals($expected['amount'], number_format($m->amount, 2), $test_name_prefix . 'Movement.amount');

        if (is_null($expected['amount_original'])) {
            $this->assertNull($m->amount_original, $test_name_prefix . 'Movement.original_amount');
        } else {
            $this->assertEquals($expected['amount_original'], number_format($m->amount_original, 2), $test_name_prefix . 'Movement.original_amount');
        }

        // Compare the obtained AccountRow with the expected one.
        if (is_null($expected['ar'])) {
            $this->assertNull($ar, $test_name_prefix . 'AccountRow');
        } else {
            $this->assertNotNull($ar, $test_name_prefix . 'AccountRow');

            // Eventually require exactly this AccountRow, by ID.
            if (isset($expected['ar']['id'])) {
                $this->assertEquals($expected['ar']['id'], $ar->id, $test_name_prefix . 'AccountRow.id');
            }

            $this->assertEquals($expected['ar']['amount'], number_format($ar->amount, 2), $test_name_prefix . 'AccountRow.amount');
            $this->assertEquals($expected['ar']['account_id'], $ar->account_id, $test_name_prefix . 'AccountRow.account_id');
        }

        return $action;
    }
}
