<?php

namespace Tests\Unit;

use PayPalAPIMovementsReader;

use PHPUnit\Framework\TestCase;

/**
 * Test Movement Importer against well-known PayPal API results.
 * @author Valerio Bozzolan
 */
class PayPalTransactionImportTest extends TestCase
{

    public const SEPARATOR_BETWEEN_TRANSACTION_AND_DETAILS = '~~~~~~ ↑ RAW TRANSACTION ↓ TRANSACTION DETAILS ~~~~~~';

    public const SEPARATOR_BETWEEN_DETAILS_AND_RESULTS     = '~~~~~~ ↑ TRANSACTION DETAILS ↓ EXPECTED RESULT ~~~~~~';

    /**
     * Open every single TXT file and open it.
     */
    public function test_paypal_files(): void
    {
        $test_directory = __DIR__ . '/paypal/' . '*.txt';
        foreach (glob($test_directory) as $filename) {
            // Unpack the test file.
            $unit_content = file_get_contents( $filename );

            // The first part of the unit test file contains the PayPal Transaction JSON.
            list( $paypal_transaction_raw, $unit_content_remaining )
              = explode( self::SEPARATOR_BETWEEN_TRANSACTION_AND_DETAILS, $unit_content, 2);

            // The second part of the unit test file contains the PayPal Transaction Details JSON,
            // and our expected result in JSON.
            list( $paypal_transaction_details_raw, $expected_raw )
              = explode( self::SEPARATOR_BETWEEN_DETAILS_AND_RESULTS, $unit_content_remaining, 2);

            // The test contains JSON contents. Parse them. Get objects.
            $paypal_transaction         = $this->jsonDecode($filename, $paypal_transaction_raw);
            $paypal_transaction_details = $this->jsonDecode($filename, $paypal_transaction_details_raw);
            $expected                   = $this->jsonDecode($filename, $expected_raw);

            // Execute the PayPal parser and get response.
            $response = [];
            $paypal_transaction = PayPalAPIMovementsReader::processSinglePayPalTransactionRaw(
              $paypal_transaction,
              $paypal_transaction_details,
              function($data) use (&$response) {
                $response = $data;
              }
            );

            // Try to give a meaningful message.
            $this->assertEquals($expected->id,           $response['id'],             "Test PayPal API parser from test file $filename: Movement ID");
            $this->assertEquals($expected->date,         $response['date'],           "Test PayPal API parser from test file $filename: Movement Date");
//          $this->assertEquals($expected->notes,        $response['notes'],          "Test PayPal API parser from test file $filename: Movement Notes");
            $this->assertEquals($expected->notes_xl,     $response['notes_xl'],       "Test PayPal API parser from test file $filename: Movement Notes XL");
            $this->assertEquals($expected->amount_gross, $response['amount_gross'],   "Test PayPal API parser from test file $filename: Movement Amount Gross");
            $this->assertEquals($expected->amount_net,   $response['amount_net'],     "Test PayPal API parser from test file $filename: Movement Amount Net");
            $this->assertEquals($expected->amount_fee,   $response['amount_fee'],     "Test PayPal API parser from test file $filename: Movement Amount Fee");
            $this->assertEquals($expected->amount_in_original_currency, $response['amount_in_original_currency'], "Test PayPal API parser from test file $filename: Movement Amount in original currency");
            $this->assertEquals($expected->currency_conversion, $response['currency_conversion'], "Test PayPal API parser from test file $filename: Movement Currency Conversion Flag");
            $this->assertEquals($expected->currency_original, $response['currency_original'], "Test PayPal API parser from test file $filename: Movement Original Currency");
        }
    }

    private function jsonDecode($filename, $txt) {
        $txt = trim( $txt );
        $response = json_decode($txt, false, JSON_THROW_ON_ERROR);
        if( !$response ) {
            throw new \Exception( "Cannot parse this JSON from file $filename:\n" . $txt );
        }
        return $response;
    }
}
