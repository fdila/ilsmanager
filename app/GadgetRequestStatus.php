<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GadgetRequestStatus extends Model
{
    use HasFactory;

    protected $fillable = ['uid', 'label', 'description'];

    /**
     * Get the gadget requests associated with the status.
     */
    public function gadgetRequests()
    {
        return $this->hasMany(GadgetRequest::class);
    }


    /**
     * Find a GadgetRequestStatus by UID.
     *
     * @param string $uid The unique identifier of the status.
     * @return GadgetRequestStatus The gadget request status with the matching UID.
     * @throws Exception If no status with the given UID is found.
     */
    public static function findByUID($uid): GadgetRequestStatus
    {
        foreach (self::all() as $status) {
            if ($status->uid === $uid) {
                return $status;
            }
        }
        throw new Exception(sprintf(
            "codice di stato di Richiesta Gadget sconosciuto: '%s'",
            $uid
        ));
    }

    /**
     * @return array
     */
    public static function getActiveStatusUIDs()
    {
        // TODO: eventually add an option in the entity itself, and query the ones with that option.
        return [
            'pending',
            'valid',
            'preparing',
        ];
    }

    /**
     * @return array
     */
    public static function getActiveStatuses()
    {
        return GadgetRequestStatus::whereIn('uid', self::getActiveStatusUIDs());
    }

    /**
     * @return array
     */
    public static function getActiveStatusIDs()
    {
        return self::getActiveStatuses()->pluck('id');
    }

}
