<?php

namespace App;

use Auth;

/**
 * Defines an Italian Linux Society model.
 * Ideally, this should be assigned to every model, but probably not mandatory.
 * It was designed to just provide some titles for each model, easy to be customized
 * from the model themselves.
 * This is not an interface, since PHP interfaces cannot have non-abstract methods.
 * This is not a class, since the "User" class already extends something else.
 * @author Valerio Bozz, 2024
 */
trait ILSModel {

    /**
     * Get the generic name of this entity, singular.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public abstract function getEntityNameSingular(): string;
    
    /**
     * Get the generic name of this entity, plural.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public abstract function getEntityNamePlural(): string;

    /**
     * Get the base view name. This is the thing before ".edit" usually.
     * This may be used to generate edit URLs.
     * @return string
     */
    public function getBaseViewName()
    {
        throw new \Exception("Not yet implemented");
    }

    /**
     * Get the object short name.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public function getObjectShortName()
    {
        // Just something :) Please override me.
        return $this->id;
    }

    /**
     * Check if the current user is the author of this object, or not.
     * Note that this MAY give extra edit visibility on this object.
     * @return bool
     */
    public final function isCurrentUserAuthor()
    {
        // At the moment do not allow anonymous editing.
        $user = Auth::user();
        return $user && $this->isUserAuthor($user);
    }

    /**
     * Check whenever a specified user is the author of this object, or not.
     * Note that this MAY give extra edit visibility on this object.
     * @param User $user Mandatory user
     * @return bool
     */
    public function isUserAuthor(User $user)
    {
        // Just something. Please override me.
        return false;
    }

    /**
     * Get the edit URL of this entity, for a normal user.
     * @return string
     */
    public function getEditURLAttribute()
    {
        // Just a sane default. Please override me.
        return $this->getAdminEditURLAttribute();
    }

    /**
     * Get the edit URL of this entity, for an admin.
     * This business logic rarely changes. So, final.
     * @return string
     */
    public final function getAdminEditURLAttribute()
    {
        return route($this->getBaseViewName() . '.edit', $this->id);
    }
}
