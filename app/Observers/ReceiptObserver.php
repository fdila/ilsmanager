<?php

namespace App\Observers;

use Mail;
use Log;
use DB;

use App\Receipt;

class ReceiptObserver
{
    public function creating(Receipt $receipt)
    {
        list($year, $month, $day) = explode('-', $receipt->date);
        $receipt->number = Receipt::where(DB::raw('YEAR(date)'), $year)->max('number') + 1;
    }

    public function saved(Receipt $receipt)
    {
        // Be sure to update the receipt accordingly to the new info.
        if ($receipt->wasChanged('causal')) {
            if ($receipt->id && file_exists($receipt->path)) {
                // It's safe to unlink this. It will be re-generated on-demand.
                unlink($receipt->path);
            }
        }
    }

    public function deleted(Receipt $receipt)
    {
        // Cleanup.
        if ($receipt->id && file_exists($receipt->path)) {
            unlink($receipt->path);
        }
    }
}
