<?php

namespace App\Observers;

use Exception;
use Log;

use App\AccountRow;
use App\Config;
use App\Fee;
use App\Section;

class AccountRowObserver
{
    /**
     * Assign an user Fee. You MUST call this only if the AccountRow has the User ID.
     */
    private function assignUserFee(AccountRow $accountRow)
    {
        // Avoid to check old data.
        if (!isset($accountRow->user)) {
            $accountRow = $accountRow->fresh();
        }

        $user = $accountRow->user;

        // Require a valid AccountRow, with a valid fee year, etc.
        try {
            $accountRow->validate();
        } catch(Exception $e) {
            // Do nothing special.
            // From the frontend we are already showing validation issues.
            return;
        }

        // Check if this user already had at least one fee.
        // This should be checked early, before saving the new Fee.
        $already_had_fee = $user->fees()->first();

        // Update the already-existing Fee, if it exists.
        $fee = $accountRow->fee;
        if (!$fee) {
            $fee = new Fee;
        }
        $fee->user_id = $accountRow->user_id;
        $fee->account_row_id = $accountRow->id;
        $fee->year = $accountRow->getFeeYear();
        $fee->save();

        // The user's section earns part of this donation.
        if ($user->section) {
            $user->section->alterBalance(Config::getSectionQuote($accountRow->amount), $accountRow, $accountRow->notes);
        }

        // Eventually activate the user and notify.
        // Note that this may be fired also when saving old quotas, so, double-check the year.
        if (!$already_had_fee && $fee->year >= date('Y')) {
            $user->sendMail('App\Mail\FirstFee');
        }
    }

    public function saved(AccountRow $accountRow)
    {
        // Consolidate the fee.
        // Note that historically it was necessary to drop them all to avoid duplicates.
        // $accountRow->fee()->delete();
        if ($accountRow->hasAccountAboutMembershipFee() && $accountRow->user_id) {
            $this->assignUserFee($accountRow);
        } else {
            $accountRow->fee()->delete();
        }

        // Consolidate the Section ID.
        if ($accountRow->wasChanged('section_id')) {
            $old_section_id = $accountRow->getOriginal('section_id');
            if (!is_null($accountRow->section)) {
                // This only works if the AccountRow has an ID. So, only in "saved()" method.
                $accountRow->section->alterBalance($accountRow->amount, $accountRow, $accountRow->notes);
            }
            elseif($old_section_id) {
                Section::where('id', $old_section_id)
                  ->first()
                  ->alterBalance($accountRow->amount * -1, null, '[Annullato] ' . $accountRow->notes);
            }
        }

        // Review again the parent Movement.
        if ($accountRow->movement) {
            $review_changed = $accountRow->movement->consolidateReviewed();
            if ($review_changed) {
                $accountRow->movement->save();
            }
        }
    }

    public function deleted(AccountRow $accountRow)
    {
        if (!is_null($accountRow->section_id)) {
            $accountRow->section->alterBalance($accountRow->amount * -1, null, '[Eliminato] ' . $accountRow->notes);
        }

        if ($accountRow->fee && $user->section) {
            $user->section->alterBalance(Config::getSectionQuote($accountRow->amount) * -1, $accountRow, '[Eliminato] ' . $accountRow->notes);
        }

        if ($accountRow->fee) {
            $accountRow->fee->delete();
        }
    }
}
