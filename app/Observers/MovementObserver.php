<?php

namespace App\Observers;

use Exception;
use Log;

use App\Movement;

class MovementObserver
{
    public function deleted(Movement $movement)
    {
        foreach($movement->account_rows as $row) {
            $row->delete();
        }
    }

    public function saving(Movement $movement)
    {
        // Auto-promote to "reviewed" only if sums make sense.
        // Or, downgrade to "to be reviewed" if a sum does not make sense.
        // Historically, a Movement was "reviewed" with at least an AccountRow.
        $movement->consolidateReviewed();
    }

}
