<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{

    use ILSModel;

    public function getEntityNameSingular(): string
    {
        return 'Banca';
    }

    public function getEntityNamePlural(): string {
        return 'Banche';
    }

    /**
     * Get the object short name.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public function getObjectShortName()
    {
        return $this->name;
    }

    public function movements()
    {
        return $this->hasMany('App\Movement');
    }

    public function rules()
    {
        return $this->hasMany('App\AccountRule');
    }

    /**
     * Scope a query to only include PayPal banks.
     */
    public function scopePaypal(Builder $query): void
    {
        // This calls scopeOfStatus().
        $query->where('type', 'paypal');
    }

    /**
     * Scope a query to only include Unicredit banks.
     */
    public function scopeUnicredit(Builder $query): void
    {
        // This calls scopeOfStatus().
        $query->where('type', 'unicredit');
    }

    public static function types()
    {
        return [
            (object) [
                'identifier' => 'paypal',
                'label' => 'PayPal'
            ],
            (object) [
                'identifier' => 'unicredit',
                'label' => 'Unicredit Banca'
            ],
        ];
    }

    public function getLastUpdateAttribute()
    {
        /*
            In fase di importazione degli estratti conto PayPal creo un
            movimento per tenere traccia delle commissioni, che però non deve
            essere considerato come ultimo movimento
        */
        if ($this->type == 'paypal') {
            return $this->movements()->where('notes', '!=', 'Commissioni PayPal')->max('date');
        }
        else {
            return $this->movements()->max('date');
        }
    }

    /**
     * Check whenever this payment is PayPal. or not.
     * @return bool
     */
    public function getIsPayPalAttribute()
    {
        return $this->type == 'paypal';
    }

    /**
     * Get the PayPal URL to pay a fee.
     * @return string
     */
    public function getPayPalFeeURL($args = [])
    {
        $user = $args['user'] ?? null;

        // Associations may have a different Fee.
        // So you can inherit from the specified user.
        $default_amount = null;
        if ($user) {
            $default_amount = $user->feeAmount;
        } else {
            $default_amount = Config::getConfig('regular_annual_fee');
        }

        $default_args = [
            'item_name' => AccountRow::createFeeNote( $user ),
            'amount' => $default_amount,
            'no_recurring' => 1, // Avoid nonsense monthly payments.
            'return' => route('thanks.member-fee.paypal'),
        ];

        $args = array_replace( $default_args, $args );
        return $this->getPayPalDonateURL($args);
    }

    /**
     * Get the PayPal URL to make a generic donation.
     * @return string
     */
    public function getPayPalDonateURL($args = [])
    {
        // Eventually customize the default item name.
        $default_item_name = "Donazione";
        if (isset($args['user'])) {
            $default_item_name = "Donazione da {$args['user']->printableName}";
            unset($args['user']);
        }

        // Some sane defaults.
        $default_args = [
            'business' => $this->identifier,
            'item_name' => $default_item_name,
            'amount' => null,
            'currency_code' => 'EUR',
            'no_recurring' => null,
            'return' => route('thanks.donation.paypal'),
        ];

        $args = array_replace( $default_args, $args );
        return 'https://www.paypal.com/donate?' . http_build_query($args);
    }
}
