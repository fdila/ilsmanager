<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    
    use ILSModel;

    public function getEntityNameSingular(): string
    {
        return 'Volontario';
    }

    public function getEntityNamePlural(): string
    {
        return 'Volontari';
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }
}
