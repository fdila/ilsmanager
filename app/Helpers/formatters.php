<?php

use Illuminate\Support\Str;

/**
 * This is just a preg_match() that screams violently
 * if the programmer is drunk. It always returns an int.
 */
function preg_match_or_throw(string $pattern, string $subject, &$matches = null, $flags = 0, $offset = 0): int
{
    $matches = null;
    $result = @preg_match($pattern, $subject, $matches, $flags, $offset);
    if ($result === false) {
        throw new Exception(sprintf(
            'The regular expression is not valid: %s',
            $pattern
        ));
    }

    return $result;
}

function mailPasswordHash($s)
{
    $salt = Str::random(10);

    return crypt($s, $salt);
}

function printableDate($value)
{
    if (is_null($value)) {
        return _i('Mai');
    } else {
        $t = strtotime($value);

        return ucwords(date('l d F o', $t));
    }
}

/**
 * Unicredit has nonsense export amount format.
 * Unicredit amounts change accordingly to your browser language.
 * This tries to understand which one is coming.
 *
 * @param  string  $raw_number  Example "1.000,00" or "1,000.00" - it MUST have decimals.
 * @return string International number like "1000.00"
 */
function parse_unicredit_number(string $raw_number): string
{
    $clean_number = $raw_number;

    $comma = substr($raw_number, -3, 1);
    if ($comma === ',') {
        // Convert from "1.000,00" to "1,000.00"
        $clean_number = str_replace('.', '', $clean_number);
        $clean_number = str_replace(',', '.', $clean_number);
    } elseif ($comma === '.') {
        // Convert from "1,000.00" to "1000.00"
        $clean_number = str_replace(',', '', $clean_number);
    } else {
        throw new Exception("Cannot parse comma from Unicredit number: '%s' cannot guess between English or Italian number", $raw_number);
    }

    return $clean_number;
}

/**
 * Format an EUR/USD amount.
 * @param float|string $v
 * @return string
 */
function format_amount(mixed $v): string
{
    // Have a valid MariaDB number for a money amount.
    // This is also a valid HTML numeric amount.
    return number_format($v, 2, '.', '');
}
