<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Log;

class Config extends Model
{
    public static function allConfig()
    {
        return [
            'association_name' => (object) [
                'label' => 'Nome Associazione',
                'type' => 'text',
                'default' => 'Italian Linux Society'
            ],
            'association_address' => (object) [
                'label' => 'Sede Legale',
                'type' => 'longtext',
                'default' => "via Aldo Moro 223\n92026 Favara (AG)\nC.F. 92043980090\nP.IVA 02438840841"
            ],
            'association_email' => (object) [
                'label' => 'Indirizzo Mail Interno',
                'type' => 'email',
                'default' => 'segreteria@linux.it'
            ],
            'donation_url' => (object) [
                'label' => 'Pagina web per ricevere donazioni',
                'type' => 'text',
                'default' => 'https://www.ils.org/info/#donazioni',
            ],
            'regular_annual_fee' => (object) [
                'label' => 'Quota Annuale Socio Ordinario',
                'type' => 'euro',
                'default' => 25
            ],
            'association_annual_fee' => (object) [
                'label' => 'Quota Annuale Socio Associazione',
                'type' => 'euro',
                'default' => 25
            ],
            'section_split' => (object) [
                'label' => 'Percentuale Quota Destinata a Sezione Locale',
                'type' => 'percentage',
                'default' => 40
            ],
            'custom_email_aliases' => (object) [
                'label' => 'Abilita alias di posta',
                'type' => 'boolean',
                'default' => 1
            ],
            'custom_email_domain' => (object) [
                'label' => 'Dominio alias di posta',
                'type' => 'text',
                'default' => 'linux.it',
                'activation' => function() {
                    return Config::getConfig('custom_email_aliases') == '1';
                }
            ],
            'custom_email_aliases_message' => (object) [
                'label' => 'Descrizione del servizio alias di posta',
                'type' => 'longtext',
                'default' => "Le mail destinate all'indirizzo @linux.it verranno conservate in una inbox sul server ILS, accessibile via IMAP sul server picard.linux.it.\nIMAP: picard.linux.it:143, STARTTLS, Password normale\nSMTP: picard.linux.it:587, STARTTLS, Password normale",
                'activation' => function() {
                    return Config::getConfig('custom_email_aliases') == '1';
                }
            ],
            'gadget_requests_realtime_notifications' => (object) [
                'label' => 'Notifiche realtime agli admin per richieste gadget',
                'type' => 'boolean',
                'default' => 1,
            ],
            'gadget_requests_multi_tracker_url_format' => (object) [
                'label' => "URL generico codici di tracciamento postale",
                'type' => 'text',
                'default' => 'https://t.17track.net/it#nums=%s',
            ],
            'custom_email_extras' => (object) [
                'label' => 'Account addizionali di posta',
                'type' => 'special',
                'default' => '[]',
                'activation' => function() {
                    return Config::getConfig('custom_email_aliases') == '1';
                }
            ],
            'refund_info_url' => (object) [
                'label' => 'Pagina di informazioni sui rimborsi',
                'type' => 'text',
                'default' => '',
            ],
            'secretary_volunteer' => (object) [
                'label' => 'Segreteria volontariale',
                'description' => 'Se hai una segreteria volontariale, seleziona questo campo, per adattare alcuni messaggi informativi.',
                'type' => 'boolean',
                'default' => 1,
            ],
            'secretary_fee_maxdays' => (object) [
                'label' => 'Max giorni di processo quota',
                'description' => 'Quanti giorni, solitamente, impiega al massimo la segreteria per importare le quote associative o simili. Utile per adattare alcuni messaggi informativi.',
                'type' => 'text',
                'default' => 15,
            ],
        ];
    }

    private static function defaultConfig($name)
    {
        $defaults = Config::allConfig();

        if (isset($defaults[$name])) {
            $config = new Config();
            $config->name = $name;
            $config->value = $defaults[$name]->default;
            return $config;
        }
        else {
            Log::error(sprintf('Richiesta configurazione non definita: %s', $name));
            return null;
        }
    }

    public static function getConfig($name)
    {
        $config = Config::where('name', $name)->first();
        if ($config == null) {
            $config = Config::defaultConfig($name);
            $config->save();
        }

        return $config->value;
    }

    public static function getSectionQuote($value)
    {
        if (empty($value)) {
            $value = self::getConfig('regular_annual_fee');
        }

        $percentage = self::getConfig('section_split');
        return round($value * $percentage / 100, 2);
    }

    public static function feesAccount()
    {
        static $cache = false;
        if ($cache === false) {
            $cache = Account::where('fees', true)->first();
        }
        return $cache;
    }
}
