<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GadgetType extends Model
{
    protected $fillable = ['name', 'is_active'];

    public function gadgetRequests()
    {
        return $this->belongsToMany(GadgetRequest::class, 'gadget_request_gadget_type', 'gadget_type_id', 'gadget_request_id');
    }
}