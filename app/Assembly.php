<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

use Auth;

class Assembly extends Model
{
    use ILSModel;

    public function getEntityNameSingular(): string {
        return 'Assemblea';
    }

    public function getEntityNamePlural(): string {
        return 'Assemblee';
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('delegate_id')->orderBy('surname', 'asc');
    }

    public function partecipating()
    {
        $user = Auth::user();
        return ($this->users()->where('user_id', $user->id)->count() != 0);
    }

    public function votations()
    {
        return $this->hasMany('App\Votation');
    }

    public function delegations($user)
    {
        $ret = new Collection();
        $ret->push($user);
        $delegating = $this->users()->where('delegate_id', $user->id)->get();
        return $ret->merge($delegating);
    }
}
