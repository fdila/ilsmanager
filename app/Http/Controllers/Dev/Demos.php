<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Provide some demo pages to developers.
 */
class Demos extends Controller
{
    public function __construct()
    {
        // Demos are available only in a local environment.
        if (! app()->environment('local')) {
            abort(404);
        }
    }

    /**
     * Provide a way to test some OAuth internal pages,
     * without setting up 10000000 servers lol.
     */
    public function oauthAuthorize(Request $request)
    {
        $args = [
            'client' => $this->createFakeOAuthClient(),
            'scopes' => [],
            'authToken' => 'DemoAuthToken',
            'request' => $request,
        ];

        return view('vendor/passport/authorize', $args);
    }

    private function createFakeOAuthClient()
    {
        return new class
        {
            public $name = 'Demo Nonsense Application Name';

            public function getKey(): string
            {
                return 'DemoOAuthKey';
            }
        };
    }
}
