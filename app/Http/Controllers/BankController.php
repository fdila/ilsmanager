<?php

namespace App\Http\Controllers;

use App\Movement;
use App\Bank;
use App\Fee;

use Illuminate\Http\Request;

class BankController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => Bank::class,
            'view_folder' => 'bank'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'name' => 'required|max:255',
            'identifier' => 'required|max:255',
        ];
    }

    protected function requestToObject(Request $request, $object): void
    {
        $object->name = $request->input('name');
        $object->type = $request->input('type');
        $object->identifier = $request->input('identifier');
    }

}
