<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Michelf\Markdown;

class DocumentationController extends Controller
{
    public function index(Request $request)
    {
        $folder = $request->input('folder', '');
        if ($folder == '/')
            $folder = '';

        $path = storage_path('documentation' . $folder);
        $dir = scandir($path);
        $files = [];

        foreach($dir as $f) {
            if ($f[0] == '.')
                continue;

            $files[] = (object) [
                'name' => $f,
                'directory' => is_dir($path . '/' . $f),
                'ext' => pathinfo($path . '/' . $f, PATHINFO_EXTENSION)
            ];
        }

        return view('documentation.index', [ "files" => $files, "folder" => $folder ]);
    }

    public function store(Request $request)
    {
        $this->checkAuth();

        $folder = $request->input('folder', '');
        if ($folder == '/')
            $folder = '';

        $path = storage_path('documentation' . $folder);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $file->move($path, $file->getClientOriginalName());
        }
        else {
            $name = $request->input('name');
            mkdir($path . '/' . $name, 0770);
        }

        return redirect()->route('doc.index', ['folder' => $folder]);
    }

    public function download(Request $request)
    {
        $file = $request->input('file');

        // Force the base path to start with a slash.
        // This is desired. If another slash is added, it's just a don't care.
        // But, in this way, you are not allowed to try to download the files
        // in the directory "documentation-something" that may exist in the future.
        $path_base = storage_path('documentation/');
        $path_file = $path_base . $file;

        // Force the request into the sandbox of our base directory.
        $real_file = realpath($path_file);
        $real_base = realpath($path_base);
        if (!$real_file || !Str::startsWith($real_file, $real_base)) {
            // Note that realpath() returns false if the file does not exist.
            // Note that we want to give a generic HTTP 403 for both cases:
            //   - unauthorized paths (traditionally, 403)
            //   - unexisting files   (traditionally, 404)
            // But we avoid to send the 404. The reason is:
            // when we receive false from realpath, we can't detect
            // if we are trying to access a legitimate missing file,
            // or any untrusted path; so, it is not wise to try to tell this
            // difference to others. Otherwise, an attacker may try to
            // to discover whenever the file "/../../../../tmp/something.txt"
            // exists or not, and try to scan the whole filesystem tree.
            // In short, please don't distinguish the "404" case, here.
            abort(403);
        }

        return response()->download($real_file);
    }

    public function link(Request $request)
    {
        $file = $request->input('file');
        $file = str_replace('..', '', $file);
        $path = storage_path('documentation' . $file);
        return response()->file($path);
    }

    public function preview(Request $request)
    {
        $folder = $request->input('folder', '');
        if ($folder == '/')
            $folder = '';
        
        $file = substr($folder, 1);
        $path = storage_path('documentation' . $folder);
        if (file_exists($path) && $path == realpath($path)) {
            if ( mime_content_type($path) === 'text/plain') {
                $text = file_get_contents($path);
                $content = Markdown::defaultTransform($text);
            } elseif ( mime_content_type($path) === 'text/html') {
                $text = file_get_contents($path);
                $content = str_replace( "\n", "<br>", strip_tags($text) );
            } elseif ( mime_content_type($path) === 'application/pdf') {
                $content = "<iframe src='" . e( route('doc.link', ['file' => '/' . $file]) ) . "' width='100%' height='500px'></iframe>";
            } else {
                $content = 'Questo non è un file visualizzabile valido.';
                $file = '404';
            }

            return view('documentation.preview', [ "content" => $content, "file" => $file, "folder" => $folder ]);
        }
        
        $content = 'Questo non è un file visualizzabile valido.';
        $file = '404';
        return view('documentation.preview', [ "content" => $content, "file" => $file, "folder" => $folder ]);
    }
}
