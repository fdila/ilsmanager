<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ThanksController extends Controller
{
    public function paidDonationViaPayPal(Request $request)
    {
        if (Auth::check()) {
            session()->put('probably_recently_paid', 'donation');
            session()->put('probably_recently_paid_via', 'paypal');
        }
        return view('thanks/donate-paypal');
    }

    public function paidMemberFeeViaPayPal(Request $request)
    {
        if (Auth::check()) {
            session()->put('probably_recently_paid', 'member.fee');
            session()->put('probably_recently_paid_via', 'paypal');
        }
        return view('thanks/donate-paypal');
    }
}
