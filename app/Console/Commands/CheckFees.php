<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

class CheckFees extends Command
{
    protected $signature = 'check:fees';
    protected $description = 'Esegue un controllo delle quote ed allerta i soci morosi';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::where('status', 'active')->get();
        $threeshold = date('Y', strtotime('-24 months'));

        foreach($users as $u) {
            $last_fee = $u->fees()->max('year');
            if ($last_fee < $threeshold) {
                $u->sendMail('App\Mail\UserFirstAlert');
            }
        }

		return 0;
    }
}
