<?php

namespace App;

use User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Section extends Model
{
    // https://laravel.com/docs/11.x/eloquent#soft-deleting
    use SoftDeletes;
    
    use ILSModel;

    public function getEntityNameSingular(): string
    {
        return 'Sezione Locale';
    }

    public function getEntityNamePlural(): string
    {
        return 'Sezioni Locali';
    }

    /**
     * Get the object short name.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public function getObjectShortName()
    {
        return "{$this->city} ({$this->prov})";
    }

    /**
     * Get the City "slug".
     * This should NOT be considered an index of any kind.
     * It was designed just for URL-cosmetic reasons.
     *
     * @return string
     */
    public function getCitySlugAttribute()
    {
        return Str::slug($this->city);
    }

    /**
     * Get the URL that describes the Registration page,
     * with this Section preloaded.
     *
     * @return string
     */
    public function getRegisterToSectionURLAttribute()
    {
        return route('register.tosection', [
            'section_id' => $this->id,
            'city_slug' => $this->citySlug,
        ]);
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Get all users that are also referents of this Section.
     *
     * @return Collection
     */
    public function getReferents()
    {
        $referents = new Collection();
        foreach ($this->users()->get() as $user) {
            if ($user->hasRole('referent')) {
                $referents->push($user);
            }
        }
        return $referents;
    }

    public function volunteers()
    {
        return $this->hasMany('App\Volunteer');
    }

    public function economic_logs()
    {
        return $this->hasMany('App\EconomicLog');
    }

    public function alterBalance($amount, $account_row, $reason)
    {
        $this->balance += $amount;
        $this->save();

        $log = new EconomicLog;
        $log->section_id = $this->id;
        $log->amount = $amount;
        $log->account_row_id = $account_row ? $account_row->id : 0;
        $log->reason = $reason;
        $log->save();
    }
}
