<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class UserSuspended extends Mailable
{
    use Queueable, SerializesModels;

    private $user = null;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        // LOL the title is like "you have a problem" so we hit the "problem-solving" mind
        // of our members to stimulate them the renewal. I know, it's evil.
        // So please don't just put "Please renew".
        return new Envelope(
            subject: 'Account sospeso'
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {

        $last_fee = $this->user->fees()->orderBy('fees.year', 'desc')->first();
        $last_fee_date = $last_fee->account_row->movement->date ?? null;

        return new Content(
            markdown: 'email.user_suspended',
            with: [
                'user' => $this->user,
                'last_fee' => $last_fee,
                'last_fee_date' => $last_fee_date,
            ],
        );
    }
}
