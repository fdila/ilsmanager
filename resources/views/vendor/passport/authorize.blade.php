<!-- DEVELOPERS: Try this page here:

    /dev/demo/oauth/authorize

-->
@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">
                        Richiesta di Autorizzazione
                    </div>
                    <div class="card-body">
                        <!-- Introduction -->
                        <p><strong>{{ $client->name }}</strong> sta richiedendo il permesso di accedere al tuo account.</p>

                        <!-- Scope List -->
                        @if (count($scopes) > 0)
                            <div class="scopes">
                                    <p><strong>Questa applicazione sarà in grado di:</strong></p>

                                    <ul>
                                        @foreach ($scopes as $scope)
                                            <li>{{ $scope->description }}</li>
                                        @endforeach
                                    </ul>
                            </div>
                        @endif

                        <div class="buttons">
                            <div class="row">
                                <div class="col">
                                    <!-- Cancel Button -->
                                    <form method="post" action="{{ route('passport.authorizations.deny') }}">
                                        @csrf
                                        @method('DELETE')

                                        <input type="hidden" name="state" value="{{ $request->state }}">
                                        <input type="hidden" name="client_id" value="{{ $client->getKey() }}">
                                        <input type="hidden" name="auth_token" value="{{ $authToken }}">
                                        <p><button class="btn">Annulla</button></p>
                                    </form>
                                </div>
                                <div class="col">
                                    <!-- Authorize Button -->
                                    <form method="post" action="{{ route('passport.authorizations.approve') }}">
                                        @csrf

                                        <input type="hidden" name="state" value="{{ $request->state }}">
                                        <input type="hidden" name="client_id" value="{{ $client->getKey() }}">
                                        <input type="hidden" name="auth_token" value="{{ $authToken }}">
                                        <p class="float-right"><button type="submit" class="btn btn-success btn-approve">Autorizza</button></p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
