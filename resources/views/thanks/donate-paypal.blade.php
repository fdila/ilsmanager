@extends('layouts.app')

@section('content')

<div class="container">
    <div class="jumbotron">
        <h1 class="display-4">Grazie per il tuo sostegno!</h1>
        <p class="lead">Verificheremo il tuo pagamento PayPal entro {{\App\Config::getConfig('secretary_fee_maxdays')}} giorni. Grazie per l'attesa!</p>

        @if (\App\Config::getConfig('secretary_volunteer'))
        <p>Ti chiediamo un po' di pazienza perché riceviamo molte mini-donazioni e simili pagamenti e il loro smistamento richiede la nostra interazione semi-manuale, nel nostro tempo libero di volontariato.</p>
        @endif

        @if (Auth::check())
        <p>Una volta processato il tuo pagamento, la tua ricevuta sarà comodamente visibile dal tuo profilo.</p>
        @else
        <p>Una volta processato il tuo pagamento, dovresti ricevere una notifica via email. Potrai anche scaricare la tua ricevuta dal tuo profilo, effettuando la registrazione e l'accesso.</p>
        @endif

        <p>Se trascorrono più di {{\App\Config::getConfig('secretary_fee_maxdays')}} giorni e ancora non vedi niente nel tuo profilo o non ricevi nessuna notifica, non esitare a contattarci! Grazie per la collaborazione!</p>

        <hr class="my-4">
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="{{route('home')}}" role="button">OK!</a>
        </p>
    </div>
</div>
@endsection