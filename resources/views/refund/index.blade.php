@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h2 class="display-4">Richieste di sostegno economico</h2>
                <p class="lead">In questa pagina chiunque può vedere <b>tutte</b> le richieste di sostegno economico o rimborsi spese ed è possibile inoltrare una nuova richiesta, seguendo il regolamento, con massima trasparenza.</p>
                <p>Alcune informazioni sono visibili solo alla segreteria e a chi è referente delle sezioni locali di riferimento.
                   Quindi, se queste informazioni non ti sono sufficienti e vuoi ulteriore trasparenza, contatta in quest'ordine: chi è richiedente, la sezione locale, o la segreteria,
                   per accedere (per esempio) ai preventivi o alle fatture che ti forniremo volentieri ma ovviamente in formato il più possibile anonimizzato, senza quindi condividere informazioni di fatturazione dei beneficiari, ecc.
                </p>
                <p>Siamo tutti invitati nel dare una mano, facendo una revisione, condividendo critiche costruttive, dando un parere nei vari canali (chat, ...) e dando soprattutto sostegno morale a questi progetti.
                   Grazie ai richiedenti che curano i loro report, grazie a chi fra voi aiuterà i richiedenti nel scriverli nei tempi previsti e grazie a chi li condivide o li commenta sui social.</p>
                <p><b>Attenzione</b>: i report sono potenzialmente <u>link esterni</u> curati dal richiedente, con politiche sulla privacy potenzialmente diverse dalla nostra.</p>
                <hr class="my-4">
                <p class="lead">
                    <a class="btn btn-primary btn-lg" href="{{ \App\Config::getConfig('refund_info_url') }}" role="button">Come funzionano i rimborsi</a>
                </p>
            </div>
        </div>
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser">Nuovo Rimborso Spese</button>
            <div class="modal fade" id="createUser" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Nuovo Rimborso Spese</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('refund.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                @include('refund.form', ['object' => null])
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-primary">Salva</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="GET" action="" class="auto-filter-form">
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="refunded">Stato:</label>

                    @foreach($all_statuses as $status)
                        <div>
                            <input id="refund-uid-{{ $status->uid }}" type="checkbox" name="status[]" value="{{ $status->uid }}" @checked(in_array($status->uid, $selected_refund_status_uids))>
                            <label for="refund-uid-{{ $status->uid }}" class="tip" title="{{ $status->description }}">{{ $status->label }} </label>
                        </div>
                    @endforeach
                </div>
            </div>

            <!-- The list of refunds is public. So, the usernames. -->
            <div class="col-md-3">
                <div class="form-group">
                    <label for="user_id">Richiedente:</label>
                    <select id="user_id" name="user_id" class="form-control">
                        <option value="">Tutti</option>

                        @foreach($users as $user)
                            <option value="{{ $user->id }}" @selected(app('request')->input('user_id') == $user->id)>{{ $user->printable_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="section_id">Sezione:</label>
                    <select id="section_id" name="section_id" class="form-control">
                        <option value="">Tutte</option>

                        @foreach($sections as $section)
                            <option value="{{ $section->id }}" @selected(app('request')->input('section_id') == $section->id)>{{ $section->city }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="year">Anno:</label>
                    <select name="year" id="year" class="form-control">
                        <option value="">Tutti</option>
                        @for($y=$max_year; $y>=$min_year; $y--)
                            <option value="{{ $y }}" @selected($year == $y)>{{ $y }}</option>
                        @endfor
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="reporturl">Report:</label>
                    <select id="reporturl" name="report" class="form-control">
                        @foreach($report_options as $value => $label)
                            <option value="{{ $value }}" @selected($value === app('request')->input('report'))>{{ $label }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                @include('commons/orderby', [
                    'availables_order_dir' => $availables_order_dir,
                    'order_dir' => $order_dir,
                ])
            </div>
            <div class="col-md-3">
                @include('commons/limit', [
                    'limit' => $limit,
                ])
            </div>
        </div>
    </form>

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Data</th>
                        <th>Titolo</th>
                        <th>Richiedente</th>
                        <th>Sezione</th>
                        <th>Importo</th>
                        <th>Da rimborsare</th>
                        <th>Stato</th>
                        <th>Report</th>
                        <th>Dettagli</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $refund)
                        <tr>
                            <td>{{ $refund->id }}</td>
                            <td>{{ $refund->date }}</td>
                            <td>{{ $refund->title ?? '' }}</td>
                            <td>{{ $refund->user ? $refund->user->printable_name : '' }}</td>
                            <td>{{ $refund->section ? $refund->section->city : '' }}</td>
                            <td class="text-right">{{ $refund->amount }}</td>
                            <td class="text-right">{{ $refund->refundStatus->completed ? '0.00' : $refund->amount }}</td>
                            <td class="text-center">
                                {{ $refund->refundStatus->label }}
                                <br>
                                <div class="progress mini-progress-bar">
                                    <div class="progress-bar bg-{{ $refund->refundStatus->color }}" role="progressbar" style="width: {{ $refund->refundStatus->percent }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </td>
                            <td>
                                @if ($refund->report_url)
                                <a class="btn" href="{{ $refund->report_url }}" target="_blank" title="Apri report del rimborso {{ $refund->id }}. {{ $refund->title }}"><i class="fa-solid fa-globe"></i></a>
                                @else
                                <abbr title="Non disponibile. Se il/la richiedente è in ritardo con la consegna. Contatta la persona per dare una mano.">n.d.</abbr>
                                @endif
                            </td>
                            <td>
                                <!-- Always show the edit link, just disable to non-admins, so it's easier to test :)
                                     We don't do security by obscurity. -->
                                <a href="{{ route('refund.edit', $refund->id) }}" class="btn {{ \Auth::user()->can('update', $refund) ? '' : 'disabled' }}"><i class="fa-solid fa-pencil"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <th colspan="4"></th>
                        <th>Totali:</th>
                        <td class="text-right">{{ format_amount($total) }}</td>
                        <td class="text-right">{{ format_amount($total_refunded) }}</td>
                        <td colspan="3"></td>
                    </tr>
                </tfoot>
            </table>
            <div class="pagination-wrapper">
                {{ $objects->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
