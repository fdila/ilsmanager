@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\Section::class)
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createSection">Registra Nuova</button>
                <div class="modal fade" id="createSection" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Sezione</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('section.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('section.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th class="align-text-top">Città</th>
                        <th class="align-text-top">Provincia</th>
                        <th class="align-text-top" title="Soci considerati attivi, oppure soci in attesa ma comunque con quota pagata">Utenze attive<br /><small>compresi soci in attesa</small></th>
                        <th class="align-text-top" title="Persone che sono in regola con la quota associativa di quest'anno">Soci in regola<br /><small>con quota {{ date('Y') }}</small></th>
                        <th class="align-text-top">Altri<br />Volontari</th>
                        <th class="align-text-top">Disponibilità</th>

                        @if($currentuser->hasRole(['admin', 'referent']))
                            <th class="align-text-top">Azioni</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $section)
                        <tr>
                            @php
                                $users_fees = $section->users()->hasFeeAtYearOrFuture()->count();
                            @endphp
                            <td>
                               @if($section->website)
                                    <a href="{{$section->website}}" target="_blank">
                                    <i class="fa-solid fa-globe" title="{{$section->website}}"></i>
                                    {{ $section->city }}
                                    </a>
                                @else
                                    {{ $section->city }}
                                @endif
                            </td>
                            <td>{{ $section->prov }}</td>
                            <td>
                                <a href="{{ route('user.index') }}?section={{$section->id}}&status=-#filters">
                                {{ $section->users()->actLikeMember()->count() }}
                                </a>
                            </td>
                            <td>
                            @if($users_fees < 3)
                                <span style="color:red">
                            @endif
                            {{ $users_fees }}
                            @if($users_fees < 3)
                                </span>
                            @endif
                            </td>
                            <td>{{ $section->volunteers()->count() }}</td>
                            <td>{{ $section->balance }} €</td>
                            @if($currentuser->hasRole(['admin', 'referent']))
                                <td>
                                    @can('update', $section)
                                        <a href="{{ route('section.edit', $section->id) }}"><i class="fa-solid fa-pencil"></i></a>
                                    @endcan
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
