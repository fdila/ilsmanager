<select name="{{ $name }}" class="form-control" @required(!empty($required)) id="{{ $id ?? 'province-' . bin2hex(random_bytes(10)) }}">
    <option value="" {{ empty($value) ? 'selected' : '' }} @disabled(!empty($required))></option>
    @foreach(allProvinces() as $prov)
        <option value="{{ $prov[0] }}" {{ $value == $prov[0] ? 'selected' : '' }}>{{ $prov[1] }}</option>
    @endforeach
</select>
