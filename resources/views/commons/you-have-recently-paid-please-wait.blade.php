<div class="alert alert-info" role="alert">
    <p class="lead">Sembra tu abbia un pagamento recente. Grazie!</p>
    <p>Processeremo il tuo pagamento entro massimo {{\App\Config::getConfig('secretary_fee_maxdays')}} giorni.</p>

    <p>Ti chiediamo un po' di pazienza perché lo smistamento delle donazioni e delle quote associative richiede la nostra interazione semi-manuale. 
    @if (\App\Config::getConfig('secretary_volunteer'))
       Le persone volontarie dietro la segreteria si prenderanno cura della tua transazione, appena avremo tempo. Grazie per l'attesa!
    @endif
    </p>
    
    <hr />
    
    <p>Se hai problemi con il tuo pagamento, soprattutto se sono ormai passati {{\App\Config::getConfig('secretary_fee_maxdays')}} giorni, contattaci e ti aiureremo!</p>
    
    <p><small>Questa notifica si auto-distruggerà al logout.</small></p>
</div>