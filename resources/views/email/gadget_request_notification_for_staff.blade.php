<!DOCTYPE html>
<html>
<head>
    <title>Nuova richiesta di spedizione gadget ({{ $count }} in attesa)</title>
</head>
<body>
<h1>Nuova richiesta di spedizione gadget</h1>
<p>È arrivata una nuova richiesta di spedizione gadget per questa persona:</p>
<ul>
    <li><strong>Nome:</strong> {{ $gadgetRequest->name }}</li>
    <li><strong>Cognome:</strong> {{ $gadgetRequest->surname }}</li>
    <li><strong>Email:</strong> {{ $gadgetRequest->whateverEmail }}</li>
</ul>
<p><a href="{{ $gadgetRequest->adminEditURL }}">Gestisci richiesta #{{ $gadgetRequest->id }}</a></p>
<p><a href="{{ route('gadget-request.index') }}" class="btn">Vedi tutte le richieste ({{ $count }} in attesa)</a></p>
</body>
</html>
