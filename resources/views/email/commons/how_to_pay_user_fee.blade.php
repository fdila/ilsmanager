<!-- This page is:
         'commons/how_to_pay_user_fee.blade.php'
     Please keep the info in sync with:
         'page/how-to-pay.blade.php'
-->
@foreach (App\Bank::all() as $bank)
@if ($bank->isPayPal)

### Come pagare con PayPal

Per pagare {{ $user->feeAmount }} EUR con PayPal, usa il seguente link rapido:

<a href="{{ $bank->getPayPalFeeURL( [ 'user' => $user ] ) }}">Paga con {{ $bank->name }}</a>

@else

### Come pagare con bonifico

Per pagare con bonifico, invia {{ $user->feeAmount }} EUR a questo IBAN in {{ $bank->name }}:

<pre>{{ $bank->identifier }}</pre>

Causale consigliata:

<pre>{{ \App\AccountRow::createFeeNote( $user) }}</pre>
@endif
@endforeach

## Cosa fare dopo il pagamento

La tua quota può richiedere fino a {{ \App\Config::getConfig('secretary_fee_maxdays') }} giorni per essere processata.

@if (\App\Config::getConfig('secretary_volunteer'))
Ti chiediamo un po' di pazienza perché il processamento richiede la nostra interazione semi-manuale, nel nostro tempo libero di volontariato.
@endif

Una volta processata, la tua ricevuta sarà visibile dal tuo profilo su [{{ config('app.name', 'ILSManager') }}]({{ route('home') }}). Puoi entrare con il tuo nome utente "{{ $user->username }}".

Se hai già pagato ed è trascorso troppo tempo e ancora non risulta la tua quota associativa, contattaci pure! Siamo qui per aiutarti.

<{{ route('page.how-to-pay') }}>

