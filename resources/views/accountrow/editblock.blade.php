<?php
$item_class = '';

$validation_exception = null;

// check if a related AccountRow was created for this Movement
if ($ar) {

    try {
        $ar->validate();
    } catch( \Exception $e ) {
        $validation_exception = $e;
    }

    if ($ar->account_id == 0)
        $item_class = 'list-group-item-danger';
    else if (!$ar->exists || $ar->edited || $validation_exception)
        $item_class = 'list-group-item-warning';
}
else {
    $item_class = 'list-group-item-danger';
}

$readonly = $readonly ?? false;
?>

<li class="list-group-item account-row-container delete-row-container {{ $item_class }}" data-delete-hidden-name="delete_account_row[]" data-delete-hidden-value="{{ $ar->id ?? '' }}">
    <input type="hidden" name="account_row[]" value="{{ $ar && $ar->exists ? $ar->id : 'new' }}">

    <div class="row">

        @if ($validation_exception)
        <div class="col-md-12">
            <p class="text-danger">{{ $validation_exception->getMessage() }}</p>
        </div>
        @endif

        <div class="col-md-3">
            <!-- AccountRow's Amount. Pay attention to do not collide the name with the Movement's Amount. -->
            <input class="form-control ils-account-amount" type="number" name="ar_amount[]" value="{{ $ar ? $ar->amountFormatted : '0.00' }}" step="0.01" @disabled($readonly)>
        </div>
        <div class="col-md-3 ils-container-account-select">
            @include('account/select', [
                'select' => $ar ? $ar->account_id : '',
                'disabled' => $readonly,
            ])
        </div>
        <div class="col-md-3 ils-container-user-select">
            <div>
                @include('user/select', [
                    'select' => $ar ? $ar->user_id : 0,
                    'status' => ['pending', 'active', 'suspended'],
                    'extra_classes' => 'ils-user-select',
                    'disabled' => $readonly,
                ])
            </div>
            <div class="d-none text-right ils-container-user-info" data-userbaseurl="{{ route('user.edit', 666) }}"><!-- please keep 666 -->
                   <p><a class="ils-user-link" href="#" target="_blank">
                   <i class="fa-solid fa-person"></i>
                   <span class="ils-user-displayname"></span>
                </a></p>
            </div>
        </div>
        <div class="col-md-3">
            @include('section/select', [
                'select' => $ar ? $ar->section_id : '',
                'disabled' => $readonly,
            ])
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ils-account-creation-info">
            Questo movimento sarà automaticamente elaborato per estrapolare più quote per l'utente selezionato. L'eventuale avanzo diventerà donazione. Gli importi e le causali sono automatiche, partendo dall'ultima quota già nota.
            Se l'utenza era sospesa, sarà ri-attivata saldando l'anno corrente.
            Se l'utenza è nuova, riceverà una <a href="https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/blob/master/resources/views/email/user_approved.blade.php?ref_type=heads" target="_blank">email di benvenuto</a>.
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-10">
            <!-- AccountRow's Notes. Pay attention to do not collide the name with the Movement's Notes. -->
            <input class="form-control ils-account-notes" type="text" name="ar_notes[]" value="{{ $ar ? $ar->notes : '' }}" placeholder="Causale" @disabled($readonly)>

            @if( $ar && $ar->fee )
            <div>
                <p><small>Anno Quota: <code>{{ $ar->fee->year }}</code> (rilevato dalla causale)</small></p>
            </div>
            @endif
        </div>

        @if (!$readonly)
            <div class="col-md-2 text-right">
                <button class="btn btn-danger delete-row" title="Elimina Riga Contabile">Elimina</button>
            </div>
        @endif
    </div>
</li>
