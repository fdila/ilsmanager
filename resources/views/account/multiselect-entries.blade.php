@foreach ($accounts as $account)
    @include('account/multiselect-entry', [
        'name' => $name,
        'value' => $account->id,
        'label' => $account->name,
        'checked' => in_array($account->id, $selected_account_ids),
        'deep' => $deep,
    ])

    @if ($account->children)
        @include('account/multiselect-entries', [
            'accounts' => $account->children,
            'selected_account_ids' => $selected_account_ids,
            'name' => $name,
            'deep' => $deep + 1,
        ])
    @endif
@endforeach