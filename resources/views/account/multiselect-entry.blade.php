@php
$name = $name ?? 'account[]';
$id = $id ?? "account-checkbox-{{ $value }}";
@endphp
<div>
<input class="form-check-input" type="checkbox" name="{{ $name }}" value="{{ $value }}" id="{{ $id }}" @checked($checked)>
<label class="form-check-label" for="{{ $id }}">{!! str_repeat('&nbsp;', $deep * 4) !!}{{ $label }}</label>
</div>