@extends('layouts.app')

@section('content')
<div class="container">
    @if($pendings->isEmpty())
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h1>Hai già revisionato tutto!</h1>
                        <p>Wow! Qui non c'è più niente da revisionare... per ora.</p>
                        <p>Quindi, concediti una bella pausa e... ci vediamo più tardi!</p>
                        <p><a href="{{ route('movement.index') }}">Torna ai Movimenti</a></p>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>Modifica e compila tutti i seguenti movimenti da revisionare. Guida rapida:</p>
                        <ul>
                            <li><b>Quote associative:</b><br />
                                   Seleziona l'utenza ma <u>senza sezione locale</u> poiché la quota non è destinata nella sua interezza alla sezione locale.</li>

                            <li><b>Donazioni:</b><br />
                                   Se possibile, selezionare l'utenza, così da archiviare la sua ricevuta. Se l'oggetto lo prevede, selezionare la relativa sezione locale.</li>

                            <li><b>Fatture fornitori:</b><br />
                                   Nella causale specifica il numero di fattura e la motivazione.</li>
                        </ul>
                    </div>
                </div>

                <!-- The bulk-edit from this list is too risky.
                <form method="POST" action="{{ route('movement.update', 0) }}">
                    @method('PUT')
                    @csrf
                    -->


                    @foreach($pendings as $movement)
                        @include('movement.editblock', [
                            'movement' => $movement,
                            'editlink' => true,
                            'readonly' => true,
                        ])
                        <br>
                    @endforeach

                    <!--
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Salva Tutti i Movimenti</button>
                        </div>
                    </div>
                    -->

                <!--
                </form>
                -->

                {!! $pendings->links() !!}
            </div>
        </div>
    @endif
</div>
@endsection
