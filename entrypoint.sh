#!/bin/sh

# Be sure to be in the right place.
if ! [ -f ".env" ]; then
	echo "ERROR: Missing .env file. That file MUST be available before starting the containers."
	exit 1
fi

# Hardening.
chmod o= .env

IS_FIRST_TIME=
RUN_COMPOSER_INSTALL=
if ! [ -f "./vendor/autoload.php" ]; then
	IS_FIRST_TIME=1
	RUN_COMPOSER_INSTALL=1
fi

# Run Composer if never executed.
if [ "$RUN_COMPOSER_INSTALL" = 1 ]; then
	composer install
fi

# Append Composer binaries in the user PATH.
# So you can easily run 'duster fix' or this kind of things.
export PATH="$PATH:./vendor/bin"

# Always migrate.
php artisan migrate

# Fix null env()
php artisan config:clear

# Always clear route cache to avoid silly problem "route not defined".
php artisan route:clear

# Run Artisan stuff if never done before.
if [ "$IS_FIRST_TIME" = 1 ]; then

	echo "[INFO] This seems the first time you execute this thing."

	# Make git happy inside the container.
	git config --global --add safe.directory /var/www/html

	php artisan key:generate
	php artisan db:seed
fi

# Check if there are no arguments.
if [ -z "$1" ]; then

	# If you are here,
	# Probably you have just executed "docker-compose up" and nothing else.

	# Import environment variables to show something nice to the user.
	if [ -f ./.env ]; then
		. ./.env
	fi

	echo " __________"
	echo "< Running! >"
	echo " ----------"
	echo "        \   ^__^"
	echo "         \  (oo)\_______"
	echo "            (__)\       )\/\ "
	echo "                ||----w |"
	echo "                ||     ||"

	if [ -n "$ILSMANAGER_EXTERNAL_PORT" ]; then

		# Generate a nice "Visit this address <link>" whatever the Docker port.
		# For interesting reasons the "port" in Docker may contain an hostname or not.
		# Is the string needing a dummy hostname?
		ILSMANAGER_EXTERNAL_HOST=
		if [ "${ILSMANAGER_EXTERNAL_PORT#*":"}" = "$ILSMANAGER_EXTERNAL_PORT" ]; then
			ILSMANAGER_EXTERNAL_HOST="localhost:"
		fi

		echo "Visit this address:"
		echo "  http://$ILSMANAGER_EXTERNAL_HOST""$ILSMANAGER_EXTERNAL_PORT""/"
	fi

	echo
	echo "  Edit the file .env to change the external port (ILSMANAGER_EXTERNAL_PORT)."
	echo
	echo "(Press CTRL+C to quit)"
	echo
	echo "IMPORTANT: Ignore the port mentioned under this message :)"

	echo "(Press CTRL+C to quit)"

	# From inside the Container, listen on all interfaces.
	# Then the user can change the external port form the compose.yaml.
	# Output ignored to avoid confusion. Since "8000" is just the internal port.
	# and the container is not aware of the external port.
	php artisan serve --host=0.0.0.0 --port=8000
else
	# The user is specifying some arguments.
	# In this case just run this as a command, as-is.
	# This is useful to users who want to run 'duster fix --dirty' or any similar thing.
	$@
fi
