<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gadget_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('is_active')->default(true);   // Campo per indicare se la distribuzione è attiva
            $table->boolean('is_default')->default(false); // Campo per indicare se il gadget è selezionato di default
            $table->timestamps();
        });

        // Esempi di valori iniziali
        DB::table('gadget_types')->insert([
            ['name' => 'Adesivi software libero', 'is_active' => true, 'is_default' => true],
            ['name' => 'Adesivi LinuxSì (per negozianti)', 'is_active' => true, 'is_default' => false],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gadget_types');
    }
};
