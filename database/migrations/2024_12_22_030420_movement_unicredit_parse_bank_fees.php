<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Take all movements without "amount_original" and populate it thanks to notes parsing.
        // If a Movement already had an AccountRow with a bank fee, refresh it.
        Artisan::call('movements:unicredit-reparse-notes');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // No rollback really. lol.
        // Maybe we can set Unicredit movements' original_amount = NULL, but too risky.
    }
};
