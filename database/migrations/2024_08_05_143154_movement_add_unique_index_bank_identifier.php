<?php

use App\Movement;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Allow nullable.
        Schema::table('movements', function (Blueprint $table) {
            $table->string('identifier', 191)->nullable()->change();
        });

        // Promote empty strings to NULL, to be able to add the unique index.
        Movement::where('identifier', '')
            ->update(['identifier' => null]);

        // Add unique index.
        // Note: this MAY suppress the index that was covering only <bank_id>
        //       and named 'movements_bank_id_foreign'.
        Schema::table('movements', function (Blueprint $table) {
            $table->unique(['bank_id', 'identifier']);
        });

        // Be sure to drop this extra legacy index covering only <bank_id>.
        try {
            Schema::table('movements', function (Blueprint $table) {
                $table->dropIndex('movements_bank_id_foreign');
            });
        } catch( Exception $e ) {
            // Probably it was alread dropped. Ignore.
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Restore the original index on <bank_id> that was suppressed by <bank_id, identifier>.
        try {
            Schema::table('movements', function (Blueprint $table) {
                $table->index('bank_id', 'movements_bank_id_foreign');
            });
        } catch( Exception $e ) {
            // Probably it's still there. Ignore.
        }

        // Drop unique.
        Schema::table('movements', function (Blueprint $table) {
            $table->dropUnique(['bank_id', 'identifier']);
        });

        // Avoid NULL values again.
        Movement::whereNull('identifier', null)
            ->update(['identifier' => '']);

        // Make not nullable again.
        Schema::table('movements', function (Blueprint $table) {
            $table->string('identifier', 191)->nullable(false)->default('')->change();
        });
    }
};
